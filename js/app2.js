function hacerPeticion() {
    const http = new XMLHttpRequest;
    const currId = document.querySelector('#input').value;
    const url = `https://jsonplaceholder.typicode.com/albums/${currId}`;
    
    http.open('GET', url, true);
	http.send();
    
    // Validar la respuesta
    if (currId > 0) {
        http.onreadystatechange = function () {
            if (this.status == 200 && this.readyState == 4) {
                // Aquí se dibuja la página
                const json = JSON.parse(this.responseText);
    
                // CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS
                document.querySelector('.output').innerHTML = json.title;
            }
            else if (this.readyState == 4) {
                alert("No encontrado o otro error");
            }
        }
    }
    else {
        alert("Número no válido");
    }
}

// Codificar los botones
document.querySelector(".btnConsultar").addEventListener("click", () => {
    hacerPeticion();
});