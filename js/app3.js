function hacerPeticion() {
	const http = new XMLHttpRequest;
	const url = "https://jsonplaceholder.typicode.com/users";
	
	http.open('GET', url, true);
	http.send();
	
	// Validar la respuesta
	http.onreadystatechange = function () {
		if (this.status == 200 && this.readyState == 4) {
			// Aquí se dibuja la página
			let res = document.getElementById("lista");
			const json = JSON.parse(this.responseText);

			// CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS
			for (const datos of json) {
				res.innerHTML += '<tr> <td class="column">' + datos.id + '</td>'
					+ '<td class="column">' + datos.username + '</td>'
					+ '<td class="column">' + datos.email + '</td>'
					+ '<td class="column">' + datos.address.street + '</td>'
					+ '<td class="column">' + datos.address.suite + '</td>'
					+ '<td class="column">' + datos.address.city + '</td>'
					+ '<td class="column">' + datos.address.zipcode + '</td>'
					+ '<td class="column">' + datos.phone + '</td>'
					+ '<td class="column">' + datos.website + '</td>'
					+ '<td class="column">' + datos.company.name + '</td> </tr>';
			}
		}
	}
}

// Codificar los botones
document.getElementById("btnCargar").addEventListener("click", () => {
	hacerPeticion();
});

document.getElementById("btnLimpiar").addEventListener("click", () => {
	let res = document.getElementById("lista");
	res.innerHTML = "";
});
