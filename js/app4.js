function hacerPeticion() {
	const http = new XMLHttpRequest;
	const currId = document.querySelector('#input').value;
	const url = `https://jsonplaceholder.typicode.com/users/${currId}`;
	
	http.open('GET', url, true);
	http.send();

	// Validar la respuesta
	if (currId > 0) {
		http.onreadystatechange = function () {
			if (this.status == 200 && this.readyState == 4) {
				// Aquí se dibuja la página
				const json = JSON.parse(this.responseText);

				// CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS
				document.querySelector('.output')
				.innerHTML =
				`
				<b>Nombre:</b> ${json.name}</br>
				<b>Email:</b> ${json.email}</br>
				<b>Teléfono:</b> ${json.phone}</br>
				<b>Website:</b> ${json.website}</br>
				`
			}
			else if (this.readyState == 4) {
				alert("No encontrado o otro error");
			}
		}
	}
	else {
		alert("Número no válido");
	}
}

// Codificar los botones
document.querySelector(".btnConsultar").addEventListener("click", () => {
	hacerPeticion();
});