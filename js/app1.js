function hacerPeticion() {
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/albums";
    
    http.open('GET', url, true);
    http.send();
    
    // Validar la respuesta
    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            // Aquí se dibuja la página
            let res = document.getElementById("lista");
            const json = JSON.parse(this.responseText);

            // CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS
            for (const datos of json) {
                res.innerHTML += '<tr> <td class="column">' + datos.userId + '</td>'
                    + '<td class="column">' + datos.id + '</td>'
                    + '<td class="column">' + datos.title + '</td> </tr>';
            }
        }
    }
}

// Codificar los botones
document.getElementById("btnCargar").addEventListener("click", () => {
    hacerPeticion();
});

document.getElementById("btnLimpiar").addEventListener("click", () => {
    let res = document.getElementById("lista");
    res.innerHTML = "";
});
