// Práctica 02 uso del Objeto fetch
// Manejando promesas
// Manejando await
llamandoFetch = () => {
  const url = "https://jsonplaceholder.typicode.com/todos";
  fetch(url)
    .then(respuesta => respuesta.json())
    .then(data => mostrarTodos(data))
    .catch((reject) => {
      console.log(`Surgió un error: ${reject}`);
    });
}

llamandoFetchAwait = async () => {
  const url = "https://jsonplaceholder.typicode.com/todos";
  // wheres God
  try {
    const respuesta = await fetch(url);
    const resultado = await respuesta.json();
    mostrarTodos(resultado);
  }
  catch(error) {
    console.log(error);
  }
}

const mostrarTodos = (data) => {
  // console.log(data);

  const res = document.getElementById('respuesta');
  res.innerHTML = '';

  // Se crea el header de la tabla
  let tableHTML = '<thead><tr>';
  Object.keys(data[0]).forEach((key) => {
    tableHTML += `<th>${key}</th>`;
  });
  tableHTML += '</tr></thead>';

  // Se crea el cuerpo de la tabla
  tableHTML += '<tbody>';
  for(let item of data) {
    tableHTML += 
    `<tr>
      <td>${item.userId}</td>
      <td>${item.id}</td>
      <td>${item.title}</td>
      <td>${item.completed}</td>
    </tr>`;
  }
  tableHTML += '</tbody>';
  
  // Se settea la tabla como la respuesta
  res.innerHTML = tableHTML;
}

document.getElementById('btnCargarP').addEventListener('click', () => {
  llamandoFetch();
});

document.getElementById('btnCargarA').addEventListener('click', () => {
  llamandoFetchAwait();
});

document.getElementById('btnLimpiar').addEventListener('click', () => {
  document.getElementById('respuesta').innerHTML = '';
});